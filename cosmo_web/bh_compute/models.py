from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.core.exceptions import ValidationError
import uuid


# Create your models here.
def limit():
    lim = models.Q(app_label='bh_compute', model='plane') | \
          models.Q(app_label='bh_compute', model='disk')
    return lim


class BlackHole(models.Model):
    spin = models.FloatField(default=0, verbose_name='Вращение')
    charge = models.FloatField(default=0, editable=False, verbose_name='Заряд')


class Observer(models.Model):
    distance = models.FloatField(default=100,  verbose_name='Расстояние')
    azimuthal_angle = models.FloatField(default=45,  verbose_name='Азимутальный угол')
    polar_angle = models.FloatField(default=0, blank=True, verbose_name='Полярный угол')


class Point(models.Model):
    x = models.FloatField(default=-70.711)
    y = models.FloatField(default=0)
    z = models.FloatField(default=-70.711)


class Surface(models.Model):
    surface_type = models.ForeignKey(ContentType, limit_choices_to=limit, on_delete=models.CASCADE)
    surface_id = models.PositiveIntegerField()
    surface_object = GenericForeignKey('surface_type', 'surface_id')

    class Meta:
        unique_together = ('surface_type', 'surface_id',)


class Disk(models.Model):
    ROTATING_CHOICES = (
        (True, '+'),
        (False, '-'),
    )
    direction = models.BooleanField(default=True, choices=ROTATING_CHOICES, verbose_name='Направление вращения')
    inner_radius = models.FloatField(default=6, verbose_name='Внутренний радиус')
    outer_radius = models.FloatField(default=12, verbose_name='Внешний радиус')
    surface = GenericRelation(Surface)


class Plane(models.Model):
    normal_vector_azimuthal_angle = models.FloatField(default=45, verbose_name='Нормаль к азимутальному углу')
    normal_vector_polar_angle = models.FloatField(default=0, verbose_name='Нормаль к полярному углу')
    point = models.ForeignKey(Point, on_delete=models.CASCADE)
    surface = GenericRelation(Surface)


class Result(models.Model):
    RESULT_TYPE_CHOICES = (
        (0, 'Изображение'),
        (1, 'Численные значения'),
        (2, 'Изображение и численные значения'),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_dt = models.DateTimeField(auto_now_add=True)
    black_hole = models.ForeignKey(BlackHole, on_delete=models.CASCADE)
    observer = models.ForeignKey(Observer, on_delete=models.CASCADE)
    surface = models.ForeignKey(Surface, on_delete=models.CASCADE)
    resolution = models.SmallIntegerField(default=256, verbose_name='Разрешение')
    accuracy = models.FloatField(default=1e-8, verbose_name='Точность')
    result_type = models.PositiveSmallIntegerField(default=2, choices=RESULT_TYPE_CHOICES, verbose_name='Тип результата')
    return_code = models.PositiveSmallIntegerField(default=0, editable=False)
    usr_email = models.CharField(blank=True, max_length=200, verbose_name='Почта')
    viewed = models.BooleanField(default=False, editable=False)


    def __str__(self):
        return str(self.id)


@receiver(pre_save, sender=Surface)
def set_package_closed(sender, instance, **kwargs):
    cr_surface = instance.surface_object
    if cr_surface is None:
        raise ValidationError('NO SUCH OBJECT')