from django.apps import AppConfig


class BhComputeConfig(AppConfig):
    name = 'bh_compute'
