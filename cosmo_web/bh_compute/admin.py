from django.contrib import admin
from .models import BlackHole, Observer, Surface, Disk, Plane, Result, Point


# Register your models here.

@admin.register(BlackHole)
class BlackHoleAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'spin', 'charge')


@admin.register(Observer)
class ObserverAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'distance', 'azimuthal_angle', 'polar_angle')


@admin.register(Point)
class PointAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'x', 'y', 'z')


@admin.register(Disk)
class DiskAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'direction', 'inner_radius', 'outer_radius')


@admin.register(Plane)
class PlaneAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'normal_vector_azimuthal_angle', 'normal_vector_polar_angle', 'point')


@admin.register(Surface)
class SurfaceAdmin(admin.ModelAdmin):
    list_display = ('surface_type', 'surface_id', 'surface_object')


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    def surface_type(self, obj):
        return obj.surface.surface_object.__str__()

    list_display = ('__str__', 'created_dt', 'black_hole', 'observer', 'surface_type', 'resolution', 'accuracy', 'result_type', 'return_code')