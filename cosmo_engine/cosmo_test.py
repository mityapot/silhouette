#!/usr/bin/env python
# coding: utf-8

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from mpl_toolkits.mplot3d import Axes3D
import math
import pickle
import pandas as pd
import re
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

from time import time

SMALL_SIZE = 15
MEDIUM_SIZE = 18
BIGGER_SIZE = 21

plt.rcdefaults()

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

mod = SourceModule("""
  __global__ void matrix_mult(int *size, double *res, double *a, double *b)
  {
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = i*size[0] + j;
    double s = 0;
    
    if( (i < size[0]) && (j< size[0]) ) {
    for (int k = 0; k < size[0]; ++k) {
        s += a[i*size[0] + k]*b[k*size[0] + j];
    }
    res[idx] = s;
    }
  }
  __global__ void matrix_sum(int *size, double *res, double *a, double *b)
  {
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    
    if( (i < size[0]) && (j< size[0]) ) {
    
        int idx = i*size[0] + j;
        res[idx] = a[idx] + b[idx];
    }
  }
  """)



func_mult = mod.get_function("matrix_mult")
func_sum = mod.get_function("matrix_sum")



def gauss_mat(locx, locy, sigx, sigy, size):
    
    x1 = np.random.randn(1024,1)
    y1 = np.random.randn(1024,1)
    
    x2 = sigx*np.random.randn(1024,1) + locx
    y2 = sigy*np.random.randn(1024,1) + locy
    
    lim1 = max(np.abs(x1).max(), np.abs(y1).max())
    lim2 = max(np.abs(x2).max(), np.abs(y2).max())

    ngrid = 100
    
    X1, Y1 = np.mgrid[-lim1:lim1:ngrid*1j, -lim1:lim1:ngrid*1j]
    positions1 = np.vstack([X1.ravel(), Y1.ravel()])
    values1 = np.vstack([x1.T, y1.T])

    kernel1 = stats.gaussian_kde(values1)
    Z1 = np.kron(np.rot90(np.reshape(kernel1(positions1).T, X1.shape)),                 np.ones((int(size/ngrid),(int(size/ngrid)))))
                         
    X2, Y2 = np.mgrid[-lim2:lim2:ngrid*1j, -lim2:lim2:ngrid*1j]
    positions2 = np.vstack([X2.ravel(), Y2.ravel()])
    values2 = np.vstack([x2.T, y2.T])

    kernel2 = stats.gaussian_kde(values2)
    Z2 = np.kron(np.rot90(np.reshape(kernel2(positions2).T, X2.shape)),                 np.ones((int(size/ngrid),(int(size/ngrid)))))
                         
    return (Z1-Z1.min())/(Z1.max()-Z1.min()),(Z2-Z2.min())/(Z2.max()-Z2.min())


## user specifies these parameters

n = 2**13
locx = 10
locy = 10
sigx = 10
sigy = 2

output = 'img' ## 'img' or 'txt'


fig,ax = plt.subplots(nrows = 1, ncols = 3, sharex=True, sharey=True, figsize=(10,10))

blockdim = 32,32
griddim = int(n/blockdim[0]), int(n/blockdim[1])
print(int(n/blockdim[0])*32,int(n/blockdim[1])*32)


a,b = gauss_mat(locx, locy, sigx, sigy, n)
print(a.shape,b.shape)

res = np.full(a.shape, 0., dtype=np.float64)
size = np.full(1, a.shape[0], dtype=np.int64)


a_gpu = cuda.to_device(a)
b_gpu = cuda.to_device(b)
size_gpu = cuda.to_device(size)
res_gpu = cuda.to_device(res)



func_sum(size_gpu, res_gpu, a_gpu, b_gpu,     block=(blockdim[0],blockdim[1],1),grid=(griddim[0],griddim[1],1))


res = cuda.from_device_like(res_gpu, res)
ax[0].imshow(a, cmap=plt.cm.terrain)
ax[1].imshow(b, cmap=plt.cm.terrain)
ax[2].imshow(res, cmap=plt.cm.terrain)

s = a + b

print(np.sum(np.abs(res - s)))


a_gpu.free()
b_gpu.free()
size_gpu.free()
res_gpu.free()

plt.tight_layout()

if output == 'img': fig.savefig('result.png')
if output == 'txt': 
    np.savetxt('result.gz', res)



