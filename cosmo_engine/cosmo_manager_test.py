import numpy as np
from scipy import stats

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from mpl_toolkits.mplot3d import Axes3D
import math
import pickle
import pandas as pd
import re
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

from time import time

class ComputeManager:
    def __init__(self, ):

